create sequence hibernate_sequence start 1 increment 1;

create table author
(
    id         int8                     not null,
    name       text                     not null,
    created_at timestamp with time zone not null,
    updated_at timestamp with time zone,
    primary key (id)
);


create table book
(
    id           int8                     not null,
    isbn         text                     not null,
    name         text                     not null,
    fk_author_id int8                     not null,
    created_at   timestamp with time zone not null,
    updated_at   timestamp with time zone,
    primary key (id),
    CONSTRAINT fk_book__author FOREIGN KEY (fk_author_id)
        REFERENCES author (id)
);