package app.springtest.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hibernate.annotations.CascadeType.ALL;

@Entity(name = "author")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Author extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "author", orphanRemoval = true, fetch = FetchType.EAGER)
    @Cascade(ALL)
    private List<Book> books = newArrayList();

    public static Author of(final String name) {
        return new Author(null, name, newArrayList());
    }
}
