package app.springtest.db;

public class AuthorNotFoundException extends RuntimeException {
    public AuthorNotFoundException() {
    }

    public AuthorNotFoundException(final String s) {
        super(s);
    }

    public AuthorNotFoundException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

    public AuthorNotFoundException(final Throwable throwable) {
        super(throwable);
    }

    public AuthorNotFoundException(final String s, final Throwable throwable, final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}
