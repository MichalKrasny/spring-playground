package app.springtest.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "book")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Book extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String isbn;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_author_id", nullable = false)
    private Author author;


    public static Book of(final String isbn, final String name, final Author author) {
        return new Book(null, isbn, name, author);
    }
}
