package app.springtest.api.book;

import app.springtest.db.AuthorNotFoundException;
import app.springtest.logging.LoggedIO;
import app.springtest.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static app.springtest.logging.LoggedIO.Level.INFO;

@RestController
@RequestMapping("api/v1/book")
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BookController {

    private final BookService bookService;

    @PostMapping
    @LoggedIO(input = INFO, result = INFO)
    public ResponseEntity upsertBook(@Valid @RequestBody BookRequest bookRequest) {
        try {
            final BookResponse response = bookService.insertBook(bookRequest);
            return ResponseEntity.ok().body(response);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

}
