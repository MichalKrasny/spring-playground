package app.springtest.api.book;

import app.springtest.api.author.AuthorResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;

@Data
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator))
@Wither
public class BookResponse {

    @JsonProperty(value = "id", required = true)
    @NotBlank
    private final Long id;

    @NotBlank
    @JsonProperty(value = "isbn", required = true)
    private final String isbn;

    @NotBlank
    @JsonProperty(value = "name", required = true)
    private final String name;

    @JsonProperty(value = "author", required = true)
    @NotBlank
    private final AuthorResponse authorResponse;

}
