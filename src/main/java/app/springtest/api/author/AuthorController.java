package app.springtest.api.author;

import app.springtest.logging.LoggedIO;
import app.springtest.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static app.springtest.logging.LoggedIO.Level.INFO;

@RestController
@RequestMapping("api/v1/author")
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorController {

    private final AuthorService authorService;


    @PostMapping
    @LoggedIO(input = INFO, result = INFO)
    public ResponseEntity newAuthor(@Valid @RequestBody AuthorRequest authorRequest) {
        return ResponseEntity.ok().body(authorService.insertAuthor(authorRequest));
    }

}
