package app.springtest.api.author;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;

@Data
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator))
@Wither
public class AuthorResponse {

    @JsonProperty(value = "id", required = true)
    @NotBlank
    private final Long id;

    @NotBlank
    @JsonProperty(value = "name", required = true)
    private final String name;

}
