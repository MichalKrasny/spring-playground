package app.springtest.service;

import app.springtest.api.author.AuthorResponse;
import app.springtest.api.book.BookRequest;
import app.springtest.api.book.BookResponse;
import app.springtest.db.*;
import app.springtest.logging.LoggedIO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;

import static app.springtest.logging.LoggedIO.Level.DEBUG;

@Slf4j
@Service
@LoggedIO(input = DEBUG, result = DEBUG)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public BookResponse insertBook(final BookRequest bookRequest) {
        @NotBlank final String isbn = bookRequest.getIsbn();
        @NotBlank final String bookName = bookRequest.getName();

        Book book = bookRepository.findByIsbn(isbn);
        //will throw exception but thats fine
        Author author = authorRepository.getByName(bookRequest.getAuthor()).orElseThrow(() -> new AuthorNotFoundException("Author not found, authorName=" + bookRequest.getAuthor()));

        if (book == null) {
            book = Book.of(isbn, bookName, author);
        } else {
            book.setName(bookName);
            book.setAuthor(author);
        }

        author.getBooks().add(book);

        Book savedBook = bookRepository.save(book);
        return new BookResponse(savedBook.getId(), savedBook.getIsbn(), savedBook.getName(), new AuthorResponse(author.getId(), author.getName()));
    }
}
