package app.springtest.service;

import app.springtest.api.author.AuthorRequest;
import app.springtest.api.author.AuthorResponse;
import app.springtest.db.Author;
import app.springtest.db.AuthorRepository;
import app.springtest.logging.LoggedIO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;

import static app.springtest.logging.LoggedIO.Level.DEBUG;

@Slf4j
@Service
@LoggedIO(input = DEBUG, result = DEBUG)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorResponse insertAuthor(final AuthorRequest authorRequest) {

        @NotBlank final String name = authorRequest.getName();

        Author author = authorRepository.findByName(name);
        if (author == null) {
            author = Author.of(name);
        }

        final Author savedAuthor = authorRepository.save(author);
        return new AuthorResponse(savedAuthor.getId(), savedAuthor.getName());
    }
}
