package app.springtest.logging;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Input parameters and method outputs will be logged. Default values for input is TRACE and for result is DEBUG.
 *
 * Example:
 * @LoggedIO(input = INFO, result = INFO)
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LoggedIO {

    enum Level {
        ERROR, WARN, INFO, DEBUG, TRACE
    }

    Level input() default Level.TRACE;

    Level result() default Level.DEBUG;
}
