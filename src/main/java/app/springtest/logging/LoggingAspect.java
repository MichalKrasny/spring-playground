package app.springtest.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    private final MethodArgumentsToStringConverter methodArgumentsToStringConverter;

    @Autowired
    public LoggingAspect(final MethodArgumentsToStringConverter methodArgumentsToStringConverter) {
        this.methodArgumentsToStringConverter = methodArgumentsToStringConverter;
    }

    @Around("within(@app.springtest.logging.LoggedIO *) && execution(* *(..))")
    public Object logAroundPublicMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Class wrappedClass = joinPoint.getSignature().getDeclaringType();
        //logging should be transparent - we want to toList logger of the wrapped class
        Logger logger = LoggerFactory.getLogger(wrappedClass.getName());

        String methodName = joinPoint.getSignature().getName();
        final String methodArguments = methodArgumentsToStringConverter.convert(joinPoint.getArgs());

        final LoggedIO loggedIoAnnotation = (LoggedIO) wrappedClass.getAnnotation(LoggedIO.class);
        writeMessage("Invoking {}({})", methodName, methodArguments, logger, loggedIoAnnotation.input());


        try {
            Object result = joinPoint.proceed();
            writeMessage("Result of {}={}", methodName, result, logger, loggedIoAnnotation.result());
            return result;
        } catch (Exception e) {
            //logger is warn, so we will not pollute incidents with these messages
            logger.warn("Intercepted exception " + e.getClass().getSimpleName() + ", while passing method \"" + methodName + "\" with arguments=" +
                    methodArguments + ": " + e.getMessage());
            throw e;
        }
    }

    private void writeMessage(final String message, final String methodName, final Object argumentOrResult, final Logger logger, final LoggedIO.Level level) {
        switch (level) {
            case ERROR:
                logger.error(message, methodName, argumentOrResult);
                break;
            case WARN:
                logger.warn(message, methodName, argumentOrResult);
                break;
            case INFO:
                logger.info(message, methodName, argumentOrResult);
                break;
            case DEBUG:
                logger.debug(message, methodName, argumentOrResult);
                break;
            case TRACE:
                logger.trace(message, methodName, argumentOrResult);
                break;
            default:
                throw new IllegalStateException("Unknown logging level: " + level);
        }
    }


}