package app.springtest.logging;

import org.springframework.stereotype.Component;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Component
public class MethodArgumentsToStringConverter {
    /**
     * @return empty string if args is null, otherwise concatenated arguments
     */
    public String convert(final Object[] args) {
        if (args == null) {
            return "";
        }

        List<StringBuilder> argumentNumberValueList = newArrayList();

        for (int i = 0; i < args.length; i++) {
            argumentNumberValueList.add(new StringBuilder(i).append(args[i] == null ? "null" : String.valueOf(args[i])));
        }
        return "(" + String.join(",", argumentNumberValueList) + ")";
    }
}
