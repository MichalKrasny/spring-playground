package app.springtest.service;

import app.springtest.api.author.AuthorRequest;
import app.springtest.api.author.AuthorRequestTestUtils;
import app.springtest.api.book.BookRequest;
import app.springtest.api.book.BookResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static app.springtest.api.book.BookRequestTestUtils.getBookRequestUnique;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceIT {

    @Autowired
    private BookService tested;

    @Autowired
    private AuthorService authorService;

    @Test
    public void insertBook_noAuthor_throwsException() {
        final BookRequest request = getBookRequestUnique();
        assertThatThrownBy(()->tested.insertBook(request)).hasStackTraceContaining("Author not found");
    }


    @Test
    public void insertBook_returnsBook() {
        final AuthorRequest authorRequest = AuthorRequestTestUtils.getAuthorRequestUnique();
        authorService.insertAuthor(authorRequest);
        final BookRequest request = getBookRequestUnique().withAuthor(authorRequest.getName());

        final BookResponse result = tested.insertBook(request);

        assertThat(result.getName()).isEqualTo(request.getName());
    }

    @Test
    public void insertBookTwice_returnsSameBook() {
        final AuthorRequest authorRequest = AuthorRequestTestUtils.getAuthorRequestUnique();
        authorService.insertAuthor(authorRequest);
        final BookRequest request = getBookRequestUnique().withAuthor(authorRequest.getName());

        final BookResponse result1 = tested.insertBook(request);
        final BookResponse result2 = tested.insertBook(request);

        assertThat(result1).isEqualTo(result2);
        assertThat(result1.getName()).isEqualTo(request.getName());
    }


    @Test
    public void insertExistingBook_bookUpdated() {
        final AuthorRequest authorRequest = AuthorRequestTestUtils.getAuthorRequestUnique();
        authorService.insertAuthor(authorRequest);
        final BookRequest request = getBookRequestUnique().withAuthor(authorRequest.getName());

        tested.insertBook(request);
        final String newName = "newName";
        final BookResponse result = tested.insertBook(request.withName(newName));

        assertThat(result.getName()).isEqualTo(newName);
    }

}