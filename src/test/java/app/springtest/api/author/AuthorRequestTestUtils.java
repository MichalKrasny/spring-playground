package app.springtest.api.author;

import java.util.UUID;

public class AuthorRequestTestUtils {

    public static final String NAME = "authorName";

    public static AuthorRequest getAuthorRequest() {
        return getAuthorRequest(1);
    }

    public static AuthorRequest getAuthorRequestUnique() {
        return getAuthorRequest(UUID.randomUUID().toString());
    }

    public static AuthorRequest getAuthorRequest(int discriminant) {
        return getAuthorRequest((String.valueOf(discriminant)));
    }

    public static AuthorRequest getAuthorRequest(String discriminant) {
        return new AuthorRequest(NAME + discriminant);
    }

}