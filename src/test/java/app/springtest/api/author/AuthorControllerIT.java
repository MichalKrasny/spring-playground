package app.springtest.api.author;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static app.springtest.api.author.AuthorRequestTestUtils.getAuthorRequestUnique;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class AuthorControllerIT {


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String url;


    @Before
    public void setUp() {
        url = "http://localhost:" + port + "/api/v1/author";
    }

    @Test
    public void postForAuthor_returnsAuthor() {
        final AuthorRequest request = getAuthorRequestUnique();

        final AuthorResponse result = this.restTemplate.postForObject(url, request, AuthorResponse.class);

        Assertions.assertThat(result.getName()).isEqualTo(request.getName());
        log.info("result={}", result);
    }

    @Test
    public void postForSameVendorAndUserButDifferentPlatform_returnsSameAuthor() {
        final AuthorRequest request = getAuthorRequestUnique();

        final AuthorResponse result1 = this.restTemplate.postForObject(url, request, AuthorResponse.class);
        final AuthorResponse result2 = this.restTemplate.postForObject(url, request, AuthorResponse.class);
        Assertions.assertThat(result1).isEqualTo(result2);
    }
}