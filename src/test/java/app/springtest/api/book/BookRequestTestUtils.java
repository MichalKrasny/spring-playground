package app.springtest.api.book;

import java.util.UUID;

public class BookRequestTestUtils {

    public static final String ISBN = "isbn";
    public static final String NAME = "bookName";
    public static final String AUTHOR = "authorName";

    public static BookRequest getBookRequest() {
        return getBookRequest(1);
    }

    public static BookRequest getBookRequestUnique() {
        return getBookRequest(UUID.randomUUID().toString());
    }

    public static BookRequest getBookRequest(int discriminant) {
        return getBookRequest((String.valueOf(discriminant)));
    }

    public static BookRequest getBookRequest(String discriminant) {
        return new BookRequest(ISBN + discriminant, NAME + discriminant, AUTHOR + discriminant);
    }

}