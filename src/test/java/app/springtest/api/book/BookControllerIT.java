package app.springtest.api.book;

import app.springtest.api.author.AuthorRequest;
import app.springtest.api.author.AuthorRequestTestUtils;
import app.springtest.api.author.AuthorResponse;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotBlank;

import static app.springtest.api.book.BookRequestTestUtils.getBookRequestUnique;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

//https://spring.io/guides/gs/testing-web/
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class BookControllerIT {


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String urlBook;
    private String urlAuthor;

    @Before
    public void setUp() {
        urlBook = "http://localhost:" + port + "/api/v1/book";
        urlAuthor = "http://localhost:" + port + "/api/v1/author";
    }

    @Test
    public void postForBook_noAuthor_badRequest() {
        final BookRequest request = getBookRequestUnique();

        ResponseEntity response = this.restTemplate.postForEntity(urlBook, request, String.class);
        log.info("result={}", response);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertThat(response.toString(), containsString("Author not found"));
    }

    @Test
    public void postForBook_returnsBook() {

        final AuthorRequest authorRequest = AuthorRequestTestUtils.getAuthorRequestUnique();
        this.restTemplate.postForEntity(urlAuthor, authorRequest, String.class);

        final BookRequest bookRequest = getBookRequestUnique().withAuthor(authorRequest.getName());
        final ResponseEntity<BookResponse> response = this.restTemplate.postForEntity(urlBook, bookRequest, BookResponse.class);

        Assertions.assertThat(response.getBody().getName()).isEqualTo(bookRequest.getName());
    }

    @Test
    public void postForBookTwice_sameBookReturned() {
        final AuthorRequest authorRequest = AuthorRequestTestUtils.getAuthorRequestUnique();
        this.restTemplate.postForEntity(urlAuthor, authorRequest, String.class);

        final BookRequest bookRequest = getBookRequestUnique().withAuthor(authorRequest.getName());
        final ResponseEntity<BookResponse> response1 = this.restTemplate.postForEntity(urlBook, bookRequest, BookResponse.class);
        final ResponseEntity<BookResponse> response2 = this.restTemplate.postForEntity(urlBook, bookRequest, BookResponse.class);

        Assertions.assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response1).isEqualTo(response2);

        final BookResponse insertedBook = response1.getBody();
        Assertions.assertThat(insertedBook.getId()).isNotNull();
        Assertions.assertThat(insertedBook.getName()).isEqualTo(bookRequest.getName());
        Assertions.assertThat(insertedBook.getIsbn()).isEqualTo(bookRequest.getIsbn());

        final @NotBlank AuthorResponse insertedAuthor = insertedBook.getAuthorResponse();
        Assertions.assertThat(insertedAuthor.getId()).isNotNull();
        Assertions.assertThat(insertedAuthor.getName()).isEqualTo(bookRequest.getAuthor());

    }
}